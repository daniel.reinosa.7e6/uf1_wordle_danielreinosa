import java.util.*

fun main() {
    val colorGris = "\u001b[37m"
    val colorGroc = "\u001b[33m"
    val colorVerd = "\u001b[32m"
    val colorRed = "\u001b[31m"
    val colorReset = "\u001b[0m"
    val scanner = Scanner(System.`in`)
    var counter = 5


    val wordList = arrayOf("BUENA", "FORMA", "PUNTO","EMULA","FOSCO","FINCA",
        "MEDIO", "MUNDO","COLOR","PIZZA","ALDEA","AHORA","COBRO","DOTAR","DIGNO",
        "DIERA","DICHO","ERIZO","ELITE","NUNCA")
    val solution = wordList.random()
    println(bienvenida())
    var userWord = scanner.next()
    while (userWord == "PLAY"){
        println("Intenta adivinar la palabra de cinco letras!")
        var selectedWord = scanner.next()
        if (counter != 0){
            for(i in 0..selectedWord.lastIndex){
                if(selectedWord == solution){
                    println("HAS GANADO CRACK")
                    return main()
                }
                else if (selectedWord[i] != solution[i] && selectedWord[i] in solution){
                    print("$colorGroc${selectedWord[i]}$colorReset") // correcte pero a un altre lloc
                }
                else if (selectedWord[i] == solution[i]) print(colorVerd+selectedWord[i]+colorReset)  // print correcte
                else if (selectedWord[i] !in solution) print(colorGris+selectedWord[i]+colorReset) // print incorrecte
                else{
                    println("Recuerda, la palabra debe tener 5 caracteres")
                    return main()
                }
            }
            counter --
            println("           -Vidas :$counter")
        }
        else if (counter == 0){
            println("Has perdido prueba de nuevo")
            println("La palabra correcta es:$solution")
            return main()
        }
    }
    while (userWord == "HELP"){
        println(". . . . . . . . . . . . . . . . . . . .W O R D L E. . . . . . . . . . . . . . . . . . . .")
        println("El objetivo del juego es adivinar una palabra concreta de cinco letras en un máximo de seis intentos.\"")
        println("Escriba en primera línea una palabra de cinco letras de su elección")
        println("El jugador escribe en la primera línea una palabra de cinco letras de su elección e introduce su propuesta.\n" +
                "                 \"\\n\" +\n" +
                "                 \"Después de cada proposición, las letras aparecen en color:\n" +
                "                 \"$colorGris El fondo gris representa las letras que no están en la palabra buscada.$colorReset\n" +
                "                 \"$colorGroc El fondo amarillo representa las letras que se encuentran en otros lugares de la palabra. $colorReset\\n\" +\n" +
                "                 \"$colorVerd El fondo verde representa las letras que están en el lugar correcto en la palabra a encontrar.$colorReset\")\n")
        println("¿Quieres empezar la partida?$colorVerd S/N $colorReset")
        var answer = scanner.next()
        if(answer == "S") return main()
        else if(answer == "N")answer == "HELP"
    }
    while (userWord != "HELP"||userWord!="PLAY"){
        return main()
    }
}
fun bienvenida(){
    println("Bienvenido a WORDLE")
    println("Indica PLAY para jugar o HELP si todavia no sabes las normas")
    println("Bona sort!!")
}
fun win(){
    println("HAS GANADO CRACK!")
    return main()
}